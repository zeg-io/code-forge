#!/usr/bin/env node
const chalk = require('chalk')
const { log, logNoDecoration } = require('../lib/log')
const cmd = require('../lib/command')()
const listCommands = require('../lib/template-helpers/listCommands')
const options = require('../lib/yargs-setup')
const chooseTemplate = require('../lib/template-helpers/choose-template')
const help = require('../lib/command/help').command

const main = async () => {
  logNoDecoration(chalk.cyan('Code Forge\n'))

  const command = options._.shift()
  const firstParam = options._[0]

  if (command) {
    if (command === 'help' && !firstParam) listCommands(cmd)
    else if (command === 'help' && firstParam) {
      logNoDecoration(chalk.bold(`${firstParam} help`))
      logNoDecoration(help(firstParam))
    } else {
      // Don't recognize the command? Show the general syntax
      if (!cmd[command])
        return log(`- Unknown command "${command}".  Use 'forge help'`)

      // EXECUTE A KNOWN GOOD COMMAND
      try {
        const result = await cmd[command].command(options)

        if (result && result.syntaxCorrection)
          return log(chalk.red(result.syntaxCorrection))
        else if (result) return log(result)
      } catch (err) {
        console.error(err)
        logNoDecoration(help(firstParam))
      }
    }
  } else {
    if (options.help || options.h || options['?']) return listCommands(cmd)
    const templateName = await chooseTemplate()

    // Initialize current directory with template
    const result = await cmd.build.command({ _: [templateName, '.'] })
    if (result) log(result)
    log('template installed.')
  }
}

main()
  .then(() => {
    process.exit(1)
  })
  .catch(err => console.error(err))
