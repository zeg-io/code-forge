# Code Forge

Code Forge is designed to allow you to easily create new scaffolded projects using your own templates.  This project was conceived after the n-th time I installed an express app using their cli only to convert all the es5 code to es6.

It is also extremely useful for companies which have specific scaffolds for new projects, and it automates several minor project setup tasks.


## Installation

```sh
npm i -g code-forge
```

## Required Tools
Code forge relies on additional cli tools which are in common use, but for the sake of documentation are listed here: git, node, and npm.

## General Features
1. Stores a list of templates (which can be initialized from a company store)
2. Initializes new projects with the code from the template selected
3. Updates the project.json file using prompts similar to `npm init` while maintaining the rest of the original project.json file.
4. Installs dependencies from the template, listed in the project.json file.

## Usage

```sh
forge <command> (options)
```

#### Commands
[build](#build)
[help](#help)
[info](#info)
[init](#init)
[load-remote](#load-remote)
[ls](#ls)
[rm](#rm)
[set](#set)

### build
_Alias for [init](#init)_

### help
List the summary version of available commands:

```sh
forge help
```

List specific help for a given command:

```sh
forge help <command>
```

### info
List template details.  For local templates this includes file count and total size for both the project files only and the entire project including the project's node_modules folder. _.gitignore affects the results_

Templates from git repos do not include size information as none is available.

```sh
forge info <template name>
```

### init 
Init retrieves the code within the template specified and installs it into the target directory if one is provided, or the current directory if none is provided. _For safety it will only do so if the target directory is empty._

```sh
forge init <template name> (target directory)

# Target the current directory
forge init company-express-template
# -or-
forge init company-express-template .

# Target a sub-directory that may or may not already exist
forge init company-express-template super-project
```


### load-remote
This command allows you to load an existing set of templates into your template database.  This is useful for teams which desire to maintain a common list of templates.

In the case of a template name conflict the command will prompt the user to pick the template source they wish to keep. It will ignore templates which are exact matches, and add any new templates to the local template database.

```sh
forge load-remote <url>
```

### ls
Lists the templates which have been installed to the local machine.

```sh
forge ls
```

### rm
Remove an existing template from the template database. _This will not remove local files or remote repos, just remove forge's reference to them._

```sh
forge rm <template name> 
```

### set
Set associates a path or git repo with a template name.  Therefore if you set a template name of `awesome-scaffold` to a directoy of `/code/scaffold-code`, the user could subsequently install all the code within `/code/scaffold-code` with the `init` command.

```sh
forge set <template name> <path or repo url>

forge set awesome-scaffold /code/scaffold-code

# Now you can install the scaffold with
cd working-code/projects
forge init awesome-scaffold new-project

# All the code will be installed 
#   the project.json will be configured 
#   and npm init will be run
```
 
## Planned Features
2. `forge build init --force template .` Add the --force option to allow installing into a directory with existing files.

## Possible Features
* Add size information to template database so that it can be displayed with `ls` and `info` even for repo templates (this would populate after the first time they were installed in the case of repos)
* Add `subscribe` command which will either
    * Check a remote source and update the local db
    * Provide a way to incorporate remote templates without downloading

## Issues
* None known.
