yargs = require('yargs')

module.exports = yargs
  .usage('Usage: forge [options]')
  .help('hidden-help')
  .option('l', { alias: 'ls', describe: 'List templates' })
  .option('i', {
    alias: 'install',
    describe: 'Install a template to an empty directory'
  })
  .option('p', {
    alias: 'path',
    describe: 'create target directory recursively'
  })
  .options('f', {
    alias: 'force',
    describe: 'force copy into directory with existing files'
  })

  .option('v', {
    alias: 'version',
    describe: 'gets the if-branch version'
  }).argv
