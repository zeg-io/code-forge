const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

const dbDir = require('os').homedir()
const dbPath = path.join(dbDir, '.codeforgedb')

const fileExists = () => {
  try {
    if (!fs.existsSync(dbDir)) fs.mkdirSync(dbDir, { recursive: true })
    if (!fs.existsSync(dbPath))
      fs.writeFileSync(
        dbPath,
        '{ "version": 1, "subscriptions": [], "templates": []}'
      )
    return true
  } catch {
    throw Error("Database file couldn't be created.")
  }
}

const getTemplates = () => {
  if (fileExists()) {
    let templateDb = JSON.parse(fs.readFileSync(dbPath, 'utf8'))
    templateDb.templates = templateDb.templates.sort((a, b) => {
      if (a.name > b.name) return 1
      if (a.name < b.name) return -1
      return 0
    })
    return templateDb
  }
}
const saveTemplates = templateDb => {
  fs.writeFileSync(dbPath, JSON.stringify(templateDb), 'utf8')
}

const getTemplateIdx = (templateDb, templateName) =>
  templateDb.templates.findIndex(t => t.name === templateName)
const getSubscriptionIdx = (templateDb, nameSpace) => {
  if (templateDb.subscriptions)
    return templateDb.subscriptions.findIndex(s => s.namespace === nameSpace)
}

module.exports.add = template => {
  const templateDb = getTemplates()
  if (template) {
    if (getTemplateIdx(templateDb, template.name) > -1) return false
    templateDb.templates.push(template)
    saveTemplates(templateDb)
    return `[${chalk.cyan(
      template.name
    )}] has been added to available templates. To use, use 'forge i ${
      template.name
    }'.`
  }
}
module.exports.update = (template, newData) => {
  const templateDb = getTemplates()
  if (template) {
    let param
    if (newData.path) param = 'path'
    if (newData.repo) param = 'repo'

    let foundIdx = getTemplateIdx(templateDb, template.name)
    templateDb.templates[foundIdx] = {
      ...templateDb.templates[foundIdx],
      repo: newData[param]
    }

    saveTemplates(templateDb)
    return `[${chalk.cyan(
      template
    )}] has been updated. To use, use 'forge i ${template}'.`
  }
}
module.exports.delete = templateName => {
  fileExists()
  const templateDb = getTemplates()
  const idx = getTemplateIdx(templateDb, templateName)

  if (idx < 0) return false
  templateDb.templates.splice(idx, 1)
  saveTemplates(templateDb)
  return true
}
module.exports.read = templateName => {
  fileExists()
  const templateJSON = getTemplates()
  if (!templateName) {
    return templateJSON.templates
  }
  return templateJSON.templates.find(template => template.name === templateName)
}

module.exports.version = () => {
  fileExists()
  const { version } = getTemplates()

  return version
}

module.exports.readSubscription = () => {
  fileExists()
  const { subscriptions } = getTemplates()

  return subscriptions || []
}
module.exports.addSubscription = subscription => {
  fileExists()
  const templateDb = getTemplates()
  if (subscription) {
    if (getSubscriptionIdx(templateDb, subscription.namespace) > -1)
      return false
    templateDb.subscriptions.push(subscription)
    saveTemplates(templateDb)
    return `[${chalk.cyan(
      subscription.namespace
    )}] has been added to available templates. To use, use 'forge i ${
      subscription.namespace
    }/template-name '.`
  }
}
module.exports.deleteSubscription = namespace => {
  fileExists()
  const templateDb = getTemplates()
  const idx = getSubscriptionIdx(templateDb, namespace)

  if (idx < 0) return false
  templateDb.subscriptions.splice(idx, 1)
  saveTemplates(templateDb)
  return true
}

module.exports.updateStats = (repoName, size) => {
  fileExists()
  const templateDb = getTemplates()
  let foundIdx = getTemplateIdx(templateDb, repoName)

  if (repoName) {
    let foundIdx = getTemplateIdx(templateDb, repoName)
    templateDb.templates[foundIdx] = {
      ...templateDb.templates[foundIdx],
      size
    }

    saveTemplates(templateDb)
    return `[${chalk.cyan(
      repoName
    )}] has been updated. To use, use 'forge info ${repoName}'.`
  }
}
