const db = require('../jsonService')

module.exports = async () => {
  const templates = db.read()
  const choices = templates.map(t => {
    return {
      name: t.name,
      message: `${t.name.padEnd(20, ' ')}${t.path || t.repo}`,
      value: t.name
    }
  })
  const { Select } = require('enquirer')
  const prompt = new Select({
    name: 'template',
    message: `Select a template to install into the current directory.
  TEMPLATE            LOCATION
  ==================  =======================================`,
    choices
  })

  return await prompt.run()
}
