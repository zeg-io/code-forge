const fs = require('fs')
const path = require('path')

module.exports.existsTemplatePath = templatePath => {
  if (!templatePath) return false
  if (!fs.existsSync(templatePath)) {
    log(
      `template source path "${templatePath}" does not exist! Delete the corrupted template and re-add it?`
    )
    return false
  }
  return true
}

module.exports.hasFiles = targetDirectory =>
  fs.readdirSync(targetDirectory).length > 0
