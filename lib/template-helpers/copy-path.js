const rcopy = require('recursive-copy')
const logUpdate = require('log-update')
const chalk = require('chalk')
const execa = require('execa')
const prettyBytes = require('pretty-bytes')

const packageJsonPrompt = require('./package-json-prompt')
const { ignore, getTemplateStats } = require('../fileHelper')
const copyOptions = require('../copy-options')
const humanReadableNumber = require('../string/humanReadableNumber')

module.exports = async ({ template, targetDirectory, options }) => {
  const templateStats = await getTemplateStats(template.path)
  const ig = ignore(template.path)
  let progress = {
    totalFiles: 0,
    totalSize: 0
  }

  return await rcopy(
    template.path,
    targetDirectory,
    copyOptions(ig, options.force)
  ).on(rcopy.events.COPY_FILE_COMPLETE, cp => {
    progress.totalFiles++
    progress.totalSize += cp.stats.size
    const showProgress = chalk
      .reset(
        '█'.repeat(Math.ceil((progress.totalSize / templateStats.bytes) * 20))
      )
      .padEnd(28, '░')

    logUpdate(
      `⸨${chalk.gray(showProgress)}⸩ : Installing template [${chalk.cyan(
        template.name
      )}]. Copied ${humanReadableNumber(
        progress.totalFiles
      )} files: ${prettyBytes(progress.totalSize)}`
    )
  })
}
