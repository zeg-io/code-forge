const fs = require('fs')
const path = require('path')
const homedir = require('os').homedir()
const npmRcFile = fs.readFileSync(path.join(homedir, '.npmrc'))
const log = require('../log')

let npmRc, author, license, version, privateModule, name, email, url

const findDefault = regex => {
  const foundDefault = npmRc.find(i => new RegExp(regex).test(i))
  let result

  if (foundDefault) result = foundDefault.split('=')[1]
  return result
}

if (npmRcFile) {
  npmRc = npmRcFile
    .toString()
    .split('\n')
    .filter(line => line.substr(0, 4) === 'init')
    .map(initItem => initItem.substring(5))

  name = findDefault(/author.name.*/)
  email = findDefault(/author.email.*/)
  url = findDefault(/author.url.*/)
  license = findDefault(/license.*/)
  privateModule = findDefault(/private.*/)
  version = findDefault(/version.*/)

  author = name
  if (email) author += ` <${email}>`
  if (url) author += ` (${url})`

  if (privateModule) privateModule = privateModule === 'true'
}

module.exports = {
  name: prompt('Package name', 'code-forge-project', projectName =>
    projectName.toLowerCase().replace(/\s/g, '-')
  ),
  description: prompt('Description'),
  version: version ? version : prompt('Version', '1.0.0'),
  author: author ? author : prompt('Author'),
  license: license ? license : prompt('License', 'ISC'),
  private: privateModule ? privateModule : '',
  repository: { type: 'git', url: prompt('Git Repo', '') }
}
