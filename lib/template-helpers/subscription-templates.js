const db = require('../jsonService')
const getRemoteTemplates = require('../template-helpers/getRemoteTemplates')

module.exports = async () => {
  let subscriptionTemplates = []
  const subscriptions = db.readSubscription()

  for (let subIdx = 0; subIdx < subscriptions.length; subIdx++) {
    const s = subscriptions[subIdx]

    const remoteTemplates = await getRemoteTemplates(s.url)
    subscriptionTemplates = subscriptionTemplates.concat(
      remoteTemplates.map(t => ({
        name: `${s.namespace}/${t.name}`,
        repo: t.repo
      }))
    )
  }
  return subscriptionTemplates
}
