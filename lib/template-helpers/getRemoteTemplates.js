const axios = require('axios')
const https = require('https')
const chalk = require('chalk')
const { log } = require('../log')

module.exports = async url => {
  try {
    // TODO: Cancel token to avoid memory leak
    const agent = new https.Agent({ rejectUnauthorized: false })
    const response = await axios.get(url, {
      httpsAgent: agent,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    // log('template list retrieved.')

    const remoteTemplateList = response.data
    if (!Array.isArray(remoteTemplateList)) {
      log(
        chalk.red(
          `aborted: Invalid file type [${
            response.headers['content-type']
          }], expected JSON array.`
        )
      )
      return false
    }
    return remoteTemplateList
  } catch (err) {
    let details = 'Network error.'
    if (err.status) details = `Status ${err.status}`
    log(`connection failed. ${details}`)
    log(chalk.red(err.message))
  }
}
