const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const packageJsonPrompt = require('./package-json-prompt')
const { log } = require('../log')

module.exports = targetDirectory => {
  log(chalk.cyan('Update package.json settings:\n'))
  return packageJsonPrompt(targetDirectory).then(packageJson => {
    if (!packageJson.repository || !packageJson.repository.url || packageJson.repository.url === '')
      delete packageJson.repository
    fs.writeFileSync(
      path.resolve(targetDirectory, 'package.json'),
      JSON.stringify(packageJson, null, 2),
      'utf8'
    )
  })
}
