const chalk = require('chalk')
const { logNoDecoration } = require('../log')
const cmd = require('../command')()

module.exports = commandObject => {
  logNoDecoration(`Usage: forge <command>, where <command> is one of:\n`)
  Object.getOwnPropertyNames(commandObject).forEach(command => {
    if (command !== 'index' && cmd[command]) {
      const { param, options, shortDescription } = cmd[command].help
      const commandDesc = options
        ? ` ${chalk.bold(options)}\n        ${shortDescription}`
        : ` ${shortDescription}`

      logNoDecoration(`${chalk.bold(param.padEnd(7))}${commandDesc}`)
    }
  })
}
