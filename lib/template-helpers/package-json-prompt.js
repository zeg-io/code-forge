const path = require('path')
const fs = require('fs')
const jsdiff = require('diff')
const pz = require('promzard')
const chalk = require('chalk')
const execa = require('execa')

const ask = targetPath =>
  new Promise((resolve, reject) => {
    if (fs.existsSync(path.resolve(targetPath, 'package.json'))) {
      const targetPackageJson = require(path.resolve(
        targetPath,
        'package.json'
      ))
      pz(
        path.resolve(__dirname, './package-json-overrides.js'),
        {},
        (err, data) => {
          if (err) {
            console.info('\n')
            if (err.message === 'canceled') {
              console.info(chalk.yellow('forge: WARN: user canceled'))
              process.exit(2)
            } else console.info('forge:', err.message)
            return reject(err)
          }

          const combined = { ...targetPackageJson, ...data }

          const diff = jsdiff.diffJson(targetPackageJson, combined)
          diff.forEach(part => {
            let color = part.added ? 'green' : part.removed ? 'red' : 'gray'
            if (!part.removed) process.stderr.write(chalk[color](part.value))
          })

          resolve(combined)
        }
      )
    } else {
      // TODO: Prompt for normal npm init?
      console.info('TODO: Prompt for npm init')
      try {
        execa.sync('npm', ['init']).stdout.pipe(process.stdout)
        resolve(true)
      } catch (err) {
        console.error(err)
      }
    }
  })
module.exports = ask
