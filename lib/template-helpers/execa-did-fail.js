module.exports = result => {
  if (result.failed || result.timedOut) process.exit(1)
  if (result.isCanceled || result.killed) process.exit(2)
}
