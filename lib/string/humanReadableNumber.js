module.exports = purportedNumber =>
  purportedNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
