const path = require('path')
const pathOrRepo = source => {
  if (source.substring(0, 4) === 'git@' || source.substring(0, 4) === 'http') {
    return { repo: source }
  } else {
    return { path: path.resolve(source) }
  }
  // return { path: source }
}

module.exports = pathOrRepo
