const fs = require('fs')
const path = require('path')
const ignoreMod = require('ignore')
const prettyBytes = require('pretty-bytes')
const logUpdate = require('log-update')
const chalk = require('chalk')
const execa = require('execa')
const util = require('util')
const countFiles = util.promisify(require('count-files'))

const db = require('../jsonService')
const {
  existsTemplatePath,
  hasFiles
} = require('../template-helpers/validation')
const { log } = require('../log')
const humanReadableNumber = require('../string/humanReadableNumber')
const syntaxFailure = require('../error-helpers/syntaxFailure')
const packageJsonPrompt = require('../template-helpers/package-json-prompt')
const packageJsonPromiseHandler = require('../template-helpers/package-json-promise-handler')
const { ignore, getTemplateStats } = require('../fileHelper')
const copyOptions = require('../copy-options')
const copyPath = require('../template-helpers/copy-path')
const execaDidFail = require('../template-helpers/execa-did-fail')

const help = {
  param: 'build',
  options: '<template name> (target directory)',
  shortDescription: 'set up template in target directory.',
  synopsis: `forge init <template name>
forge init <template name> <target directory>

ALIAS: build`,
  description: `This command copies the files and directory structure of a template into the target
directory, propmts to override the package.json, then installs npm packages.`
}

const command = async options => {
  const templateName = options._[0]
  const targetDirectory = options._[1] || '.'
  const recursiveMkDir = options.p || options.path || false
  const forceCopyIntoDirtyDirectory = options.force || options.f || false

  if (templateName && targetDirectory) {
    const template = db.read(templateName)

    if (!template)
      return log(
        `"${templateName}" template does not exist. Use "forge ls" to list available templates.`
      )
    if (existsTemplatePath(template.path)) {
      // create target directory if it doesn't exist yet, with --path if passed
      if (!fs.existsSync(targetDirectory)) {
        let recursive = {}
        if (options.path) recursive = { recursive: recursiveMkDir }
        fs.mkdirSync(targetDirectory, recursive)
      } else if (hasFiles(targetDirectory) && !forceCopyIntoDirtyDirectory)
        return log(
          'files exist inside the target directory, aborted! To force overwrite use --force parameter'
        )

      return await copyPath({ template, targetDirectory, options })
        .then(() => packageJsonPromiseHandler(targetDirectory))
        .then(() =>
          execa('npm', ['i'], { cwd: targetDirectory, stdio: 'inherit' })
        )
        .then(result => {
          if (result.exitCode === 0) return
          process.exit(result.exitCode)
        })
        .catch(err => console.error(err))
    } else if (template.repo) {
      const gitCommand = `git clone ${template.repo} ${targetDirectory}`
      log(gitCommand)
      const ig = ignoreMod().add('.git')

      let repoStats
      return execa('git', ['clone', template.repo, targetDirectory], {
        stdio: 'inherit'
      })
        .then(execaResult => {
          execaDidFail(execaResult)
          return packageJsonPromiseHandler(targetDirectory)
        })
        .then(async () => {
          repoStats = await getTemplateStats(targetDirectory)
          return execa('npm', ['i'], { cwd: targetDirectory, stdio: 'inherit' })
        })
        .then(execaResult => {
          execaDidFail(execaResult)

          return countFiles(targetDirectory, {
            ignore: file => ig.ignores(path.relative(targetDirectory, file))
          })
        })
        .then(installedStats => {
          log(`${chalk.bold(chalk.green('Template Installed'))}

Project Files : ${chalk.cyan(humanReadableNumber(repoStats.files))}
Project Size  : ${chalk.cyan(prettyBytes(repoStats.bytes))}

Total Files   : ${chalk.cyan(humanReadableNumber(installedStats.files))}
Total Size    : ${chalk.cyan(prettyBytes(installedStats.bytes))}`)
          db.updateStats(templateName, {
            projectFiles: repoStats.files,
            projectSize: repoStats.bytes,
            totalFiles: installedStats.files,
            totalSize: installedStats.bytes
          })
        })
    } else log('fatal error in build.')
    // check to see if targetDirectory exists
  } else if (!templateName && !targetDirectory) {
    syntaxFailure(help)
  } else if (!targetDirectory) {
    log('missing target directory.')
  }
}

module.exports = {
  help,
  command
}
