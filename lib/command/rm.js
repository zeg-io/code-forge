const db = require('../jsonService')
const { log } = require('../log')
const pathOrRepo = require('../pathOrRepo')
const getTemplateName = require('../getTemplateName')

const help = {
  param: 'rm',
  options: '<template name>',
  shortDescription: 'Removes a template from the forge template list.',
  synopsis: `forge rm <template name>`,
  description:
    'This command removes an existing template from the forge template list.'
}

const command = options => {
  const templateName = options._[0]

  let template = db.read(templateName)

  if (!template) return log('template not found.')
  if (db.delete(templateName)) return 'template deleted.'
  return 'error deleting template!'
}

module.exports = {
  help,
  command
}
