const fs = require('fs'),
  path = require('path')

const cmd = () => {
  const commands = fs
    .readdirSync(__dirname)
    .map(f => f.substring(0, f.indexOf('.')))

  let cmd = {}
  commands.forEach(c => {
    cmd[c] = require(path.join(__dirname, `${c}.js`))
  })
  return cmd
}

module.exports = cmd
