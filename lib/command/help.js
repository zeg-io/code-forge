const cmd = require('./index')()
const helpRenderer = require('../helpRenderer')

module.exports.help = {
  param: 'help',
  options: '(command)',
  shortDescription: 'Self help for the helpless.',
  synopsis: `forge help (with no args)`,
  description:
    'This command lists the currently installed templates, whether they are folders or repos.'
}

module.exports.command = (helpTopic = 'help') => {
  if (!cmd[helpTopic]) return 'Proper syntax is `forge help <command>`'
  if (cmd[helpTopic].hasOwnProperty('help'))
    return helpRenderer(cmd[helpTopic].help)
  return `ERROR: No help exists for the "${helpTopic}" command`
}
