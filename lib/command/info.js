const chalk = require('chalk')
const ignoreMod = require('ignore')
const util = require('util')
const path = require('path')

const syntaxFailure = require('../error-helpers/syntaxFailure')
const db = require('../jsonService')
const subscriptionTemplates = require('../template-helpers/subscription-templates')
const humanReadableNumber = require('../string/humanReadableNumber')

const countFiles = util.promisify(require('count-files'))
const prettyBytes = require('pretty-bytes')
const { getTemplateStats } = require('../fileHelper')

const help = {
  param: 'info',
  options: '<template name>',
  shortDescription: 'Get information about a template.',
  synopsis: `forge info <template name>`,
  description: `This command displays details about a given template.`
}

const command = async options => {
  const templateName = options._[0]

  if (!templateName) return syntaxFailure(help)

  let template = db.read(templateName)
  if (!template) {
    const subTemplates = await subscriptionTemplates()
    template = subTemplates.find(t => t.name === templateName)
  }

  if (!template)
    return `template [${chalk.cyan(
      templateName
    )}] could not be found. Use 'forge ls' to list available templates.`

  const ig = ignoreMod().add('.git')
  const templatePath = template.path

  const description = template.description
    ? `Description   : ${template.description}\n`
    : ''
  const defineTotal = chalk.dim(`Note: The "total" numbers include node_modules and other files that .gitignore ignores, 
      but which make up the project as a whole.`)
  const noTotals = chalk.dim(`This template is a remote repo and as such the size cannot be calculated. After installing
a repo template total size will be displayed.`)
  let sizes
  if (template.size) {
    sizes = chalk.italic(`
Project Files : ${chalk.cyan(humanReadableNumber(template.size.projectFiles))}
Project Size  : ${chalk.cyan(prettyBytes(template.size.projectSize))}

Total Files   : ${chalk.cyan(humanReadableNumber(template.size.totalFiles))}
Total Size    : ${chalk.cyan(prettyBytes(template.size.totalSize))}

These are numbers provided by the subscription namespace`)
  }

  if (templatePath) {
    const templateStats = await countFiles(templatePath, {
      ignore: file => ig.ignores(path.relative(templatePath, file))
    })
    const projectStats = await getTemplateStats(templatePath)

    return `INFO

Template      : ${chalk.cyan(templateName)}
Source        : ${chalk.yellow(templatePath)}
${description}
Project Files : ${chalk.cyan(humanReadableNumber(projectStats.files))}
Project Size  : ${chalk.cyan(prettyBytes(projectStats.bytes))}

Total Files   : ${chalk.cyan(humanReadableNumber(templateStats.files))}
Total Size    : ${chalk.cyan(prettyBytes(templateStats.bytes))}

${defineTotal}`
  } else {
    return `INFO

Template      : ${chalk.cyan(templateName)}
Source        : ${chalk.cyan(template.repo)}
${description}${sizes + '\n\n' + defineTotal || noTotals}`
  }
}

module.exports = {
  help,
  command
}
