const fs = require('fs')
const path = require('path')
const db = require('../jsonService')
const { log } = require('../log')
const pathOrRepo = require('../pathOrRepo')
const syntaxFailure = require('../error-helpers/syntaxFailure')

const help = {
  param: 'set',
  options: '<template name> <path|repo>',
  shortDescription: 'Add a template to the forge template list.',
  synopsis: `forge set <folder>
forge set <git repo>`,
  description:
    'This command sets the value of template to point at paths or repos to the forge template list.'
}

const command = options => {
  const name = options._[0]
  const source = options._[1]

  if (
    source.substring(0, 4).toLowerCase() !== 'http' &&
    !fs.existsSync(path.resolve(source))
  )
    return log('source directory does not exist.')

  if (name && source) {
    const template = {
      name,
      ...pathOrRepo(source)
    }
    const message = db.add(template)

    if (!message) return log('template already exists')
    return message
  } else if (!name && !source) syntaxFailure(help)
  else if (!source) log('missing template source')
}

module.exports = {
  help,
  command
}
