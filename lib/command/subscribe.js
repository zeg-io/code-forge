const db = require('../jsonService')
const chalk = require('chalk')
const { log } = require('../log')
const syntaxFailure = require('../error-helpers/syntaxFailure')

const help = {
  param: 'subscribe',
  options: '<namespace> <url>',
  shortDescription:
    'NOT IMPLEMENTED: Add a remote template database to the available templates.',
  synopsis: `forge subscribe <namespace> <url>
forge subscribe <url>`,
  description:
    'This command IS NOT YET IMPLEMENTED makes a remote template db available to your client.'
}

const command = options => {
  let url = options._[1]
  let namespace = options._[0]

  // Force non-namespace subscriptions into r/ namespace
  const sub = {
    namespace: url ? namespace : 'r',
    url: url ? url : namespace
  }

  if (!namespace && !url) return syntaxFailure(help)

  db.addSubscription(sub)
  return log(
    `Subscribed [${chalk.cyan(sub.namespace)}] namespace to [${chalk.cyan(
      sub.url
    )}]`
  )
}

module.exports = {
  help,
  command
}
