const build = require('./build')
module.exports = {
  help: { ...build.help, param: 'init' },
  command: build.command
}
