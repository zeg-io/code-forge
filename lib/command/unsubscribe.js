const db = require('../jsonService')
const chalk = require('chalk')
const { log } = require('../log')
const syntaxFailure = require('../error-helpers/syntaxFailure')

const help = {
  param: 'unsubscribe',
  options: '<namespace> <url>',
  shortDescription:
    'Remove a remote template namespace from available templates.',
  synopsis: `forge unsubscribe <namespace>`,
  description:
    'This command removes a remote template namespace from available templates.'
}

const command = options => {
  let namespace = options._[0]

  if (!namespace) return syntaxFailure(help)
  if (namespace.includes('/'))
    return log(
      `unsubscribe requires you pass in only the namespace, you passed in [${chalk.cyan(
        namespace
      )}].\nDid you mean: ${chalk.green(
        `forge unsubscribe ${namespace.substr(0, namespace.indexOf('/'))}`
      )}`
    )

  db.deleteSubscription(namespace)
  return log(`Unsubscribed the [${chalk.cyan(namespace)}] namespace.`)
}

module.exports = {
  help,
  command
}
