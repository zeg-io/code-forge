const chalk = require('chalk')
const db = require('../jsonService')
const { log, logNoDecoration } = require('../log')
const helpRenderer = require('../helpRenderer')
const subscriptionTemplates = require('../template-helpers/subscription-templates')

const help = {
  param: 'ls',
  shortDescription: 'Lists the currently installed templates.',
  synopsis: `forge ls (with no args)`,
  description:
    'This command lists the currently installed templates, whether they are folders or repos.'
}

const command = async () => {
  const templates = db.read().concat(await subscriptionTemplates())

  if (templates.length === 0)
    log(
      chalk.red(
        'No templates have been added yet.  Add with `forge add <directory/repo>'
      )
    )
  else {
    logNoDecoration('TEMPLATE            LOCATION')
    logNoDecoration(
      '==================  ======================================='
    )
    templates.forEach(t => {
      const name = t.name.includes('/')
        ? chalk.blue(t.name.padEnd(20))
        : t.name.padEnd(20)

      logNoDecoration(
        `${name}${t.path ? chalk.yellow(t.path) : chalk.cyan(t.repo)}`
      )
    })
  }
}

module.exports = {
  help,
  command
}
