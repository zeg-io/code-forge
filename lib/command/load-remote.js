const chalk = require('chalk')
const ignoreMod = require('ignore')
const axios = require('axios')
const https = require('https')
const enquirer = require('enquirer')

const syntaxFailure = require('../error-helpers/syntaxFailure')
const getRemoteTemplates = require('../template-helpers/getRemoteTemplates')
const db = require('../jsonService')
const { log } = require('../log')

const help = {
  param: 'load-remote',
  options: '<template list url>',
  shortDescription: 'Merge a remote template list into your templates.',
  synopsis: `forge load-remote <template list url>`,
  description: `This command loads a remote json file and merges it into your local template list. 
This is a great way to share team template lists.`
}

const command = async options => {
  const url = options._[0]

  if (!url) return syntaxFailure(help)

  try {
    log('connecting to remote server...')
    const remoteTemplateList = await getRemoteTemplates(url)
    let newTemplates = 0,
      updatedTemplates = 0

    for (let i = 0; i < remoteTemplateList.length; i++) {
      const remoteTemplate = remoteTemplateList[i]
      const localTemplate = db.read(remoteTemplate.name)

      if (localTemplate) {
        if (localTemplate.repo !== remoteTemplate.repo) {
          const { Select } = require('enquirer')
          const prompt = new Select({
            name: 'template',
            message: `Duplicate [${chalk.cyan(
              remoteTemplate.name
            )}] template detected, which repo would you like to use?`,
            choices: [
              {
                name: `local  ${localTemplate.repo}`,
                value: localTemplate.repo
              },
              {
                name: `remote ${remoteTemplate.repo}`,
                value: remoteTemplate.repo
              }
            ]
          })
          const repo = await prompt.run()

          if (repo !== localTemplate.repo) {
            updatedTemplates++
            log(
              db.update(remoteTemplate.name, {
                repo
              })
            )
          }
        }
      } else {
        newTemplates++
        db.add(remoteTemplate)
      }
    }
    if (newTemplates > 0) log(`Added ${chalk.cyan(newTemplates)} template(s).`)
    if (updatedTemplates > 0)
      log(`Updated ${chalk.cyan(updatedTemplates)} template(s).`)
    if (newTemplates + updatedTemplates === 0)
      log(chalk.yellow('No changes made to the template list.'))
  } catch (err) {
    let details = 'Network error.'
    if (err.status) details = `Status ${err.status}`
    log(`connection failed. ${details}`)
    log(chalk.red(err.message))
  }
}

module.exports = {
  help,
  command
}
