const chalk = require('chalk')
const helpRenderer = ({
  param,
  shortDescription,
  synopsis = '',
  description = ''
}) => {
  const out = `
${chalk.bold('NAME')}
       ${chalk.gray.bold(`forge-${param}`)} - ${shortDescription}

${chalk.bold('SYNOPSIS')}
       ${synopsis.replace(/\n/g, '\n       ')}

${chalk.bold('DESCRIPTION')}
       ${description.replace(/\n/g, '\n       ')}
`
  return out
}

module.exports = helpRenderer
