const path = require('path')
const getTemplateName = source => {
  let s = source
  if (s.substring(0, 1) === '.') {
    s = path.resolve(s)
  }
  let lastSlash = s.lastIndexOf('/')
  if (lastSlash < 0) {
    lastSlash = s.lastIndexOf('\\')
  }
  return s.substring(lastSlash + 1)
}

module.exports = getTemplateName
