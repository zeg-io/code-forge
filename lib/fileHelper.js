const fs = require('fs')
const path = require('path')
const util = require('util')
const ignoreMod = require('ignore')

const countFiles = util.promisify(require('count-files'))

const ignore = templatePath => {
  const gitignorePath = path.resolve(templatePath, '.gitignore')

  if (fs.existsSync(gitignorePath)) {
    return ignoreMod()
      .add(() => fs.readFileSync(gitignorePath, 'utf8'))
      .add('.git')
  } else return ignoreMod().add('.git')
}

const fileHelper = async templatePath => {
  const ig = ignore(path.resolve(templatePath))

  return await countFiles(templatePath, {
    ignore: file => ig.ignores(path.relative(templatePath, file))
  })
}

module.exports = { getTemplateStats: fileHelper, ignore }
