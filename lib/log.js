module.exports.log = message => {
  this.logNoDecoration(`forge: ${message}`)
}
module.exports.logNoDecoration = message => {
  process.stdout.write(`${message}\n`)
}
