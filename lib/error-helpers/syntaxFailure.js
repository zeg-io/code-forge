const { log } = require('../log')

module.exports = help => {
  log(`fatal: ${help.param} requires syntax "${help.param} ${help.options}"`)
}
