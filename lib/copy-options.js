module.exports = (ignorer, forceCopy) => ({
  overwrite: forceCopy,
  dot: true,
  filter: file => {
    if (file && file !== '') return !ignorer.ignores(file)
    return false
  }
  // ['**/*', '!.git', '!node_modules/**/*', '!npm-debug.log']
})
